pragma solidity 0.4.24;


import "./Ownable.sol";



 /**
 * @title SuperAdmin
 */
 
contract superAdmin is  Ownable 
{
 
 constructor() Ownable(msg.sender) public {}
 
 mapping(address => bool ) public validAdmin;
 uint256 public noOfAdmins = 0;
 

 //function to add admins 
 function addAdmin(address[] _Admin) public onlyOwner {
    uint arrayLength = _Admin.length;
        
        for (uint x = 0; x < arrayLength; x++) 
            { 
                 validAdmin[_Admin[x]] = true ;
                 noOfAdmins = noOfAdmins+1;
            }
 
     
 }
  //function to remove admins
  function removeAdmin(address _Admin) public onlyOwner {
    require(validAdmin[_Admin]); 
    validAdmin[_Admin] = false ;
    noOfAdmins = noOfAdmins-1;
 }
 // to check is ethereum address is admin or not
  function isAdmin(address _Admin) public view returns(bool) {
      return validAdmin[_Admin];
  }

}