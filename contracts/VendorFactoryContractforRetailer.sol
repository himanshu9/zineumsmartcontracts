pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

import "./SafeMath.sol";
import "./Ownable.sol";
import "./VendorCampaignForRetailer.sol";


 
contract VendorFactoryContractforRetailer is Ownable 
{
    address public feeWallet;
    using SafeMath for uint256;
      struct Campaign {
        uint256 id;
        address seller;
        uint256 tokens;
        uint256 numOfProducts;
        uint256[] allocationOfTokens;
        uint256[] idOfProducts;
        uint[] rewardsOnProducts;
        uint256 zinTokens;
        uint256 reserveTokens;
    }

    // mapping id of campaign to detail struct of campaign
    mapping(address => Campaign) public CampaignDetails;
    event feeTransfer(address _wallet, uint256 _amount);
    event zinTransfer(address _contract, uint256 _value);
    //address of a campaign exits or not
    mapping(address => bool) public campaigns;
    //licence mapping,returns true if licence for an address exists and false if licence is revoked or doesnot exist;
    mapping(address => bool) public licences;
    //mapping of campaign id vs address
    mapping(uint256 => address) public campaignIdVsAddress;
    // no of retailers
    uint256 public numOfRetailers;
 
    modifier isLicenced(address ad) {
        require(licences[ad], "not licenced");
        _;
    }

    constructor() Ownable(msg.sender) public {}

    //function to create vendor campaign  for retailer    
    function createVendorCampaignRetailer(
        uint256 campaignId, 
        uint256 _tokens,//  ryt now Token is no of Zin Tokens 
        uint256[] _allocationOfTokens,
        uint256[] productIds,
        uint256 duration,
        uint[] rewardsOnProducts
        
    ) payable isLicenced(msg.sender)  public  returns(address)
    {
        require(feeWallet != address(0),"wallet address is null");
        uint256 reserveTokens = _allocationOfTokens[0];
        uint256 zinTokens =  _allocationOfTokens[1];
        address campaignAddress = new VendorCampaignForRetailer(msg.sender,  _allocationOfTokens,zinTokens,reserveTokens,productIds,campaignId,duration,rewardsOnProducts); 
        campaigns[campaignAddress] = true;
        CampaignDetails[campaignAddress] = Campaign({
            id: campaignId,
            seller: msg.sender,
            tokens: reserveTokens.add(zinTokens), 
            numOfProducts: productIds.length,
            allocationOfTokens : _allocationOfTokens,
            idOfProducts : productIds,
            rewardsOnProducts: rewardsOnProducts,
            zinTokens: zinTokens,
            reserveTokens:reserveTokens
        });
        
        campaignIdVsAddress[campaignId] = campaignAddress;
        feeWallet.transfer(msg.value.mul(3).div(100));
        campaignAddress.transfer(msg.value.sub(msg.value.mul(3).div(100)));
        return campaignAddress;
        emit feeTransfer(feeWallet,msg.value.mul(3).div(100));
        emit zinTransfer(campaignAddress,msg.value.sub(msg.value.mul(3).div(100)));
    }   
    //to set address to collect fees while creating campaign called by owner only
    function setFeeWallet(address _wallet) public onlyOwner returns(address)  {
        require(_wallet != 0x0,"wallet address is null");
        feeWallet = _wallet;
        return feeWallet;
    }
    //allocate  licences of reatilers 
    function allocateLicence(address retailer) public onlyOwner{
      require(!licences[retailer]);
      licences[retailer] = true;
      numOfRetailers ++;
   }
    //revoke licences of reatilers 
   function revokeLicence(address retailer) public onlyOwner{
       require(licences[retailer]);
       licences[retailer] = false;
      
   }

   function (){
       revert();
       
   }
}