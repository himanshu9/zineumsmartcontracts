pragma solidity 0.4.24;
import './SafeMath.sol';
import "./Ownable.sol";
contract ReserveWallet is Ownable {
     using SafeMath for uint256;
    mapping (address => uint256) userTokens;

//to be add later only contract can call these functions

    constructor() Ownable(msg.sender) public{
        
    }
    //function to allocate Reserve tokens to user
    function allocateTokensToUser(address user,uint256 tokens) public {
        userTokens[user] = userTokens[user].add(tokens);
    }
    //function to Redeem Reserve tokens of user    
    function redeemUserTokens(address user,uint256 tokens) public {
        userTokens[user] = userTokens[user].sub(tokens);
    }
    //function to check Reserve tokens of user    
     function checkUserTokens(address user) public view returns(uint256){
        return userTokens[user];
    }
}