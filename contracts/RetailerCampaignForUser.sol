pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;
pragma experimental "v0.5.0";
import "./SafeMath.sol";
import "./Ownable.sol";

contract superAdmin {
  function isAdmin(address _Admin) public view returns(bool);
}

contract ReserveWallet {
    function allocateTokensToUser(address user,uint256 tokens) public;
    function checkUserTokens(address user) public view returns(uint256);
}

contract RetailerCampaignForUser is Ownable 
{
    using SafeMath for uint256;
    uint256 public campaignId;// campaign ID for current campaign
    uint256 public tokensLeft;// tokens left in campaign both reserve and Zin
    uint256 public minimumSuccessTokens;//minimum tokens to declare sucess of campaign
    uint256 public tokensAvailed;//tokens availed from campaign
    uint256 public zinPerUser;// no of zin tokens per user
    uint256 public reserveTokensPerUser;//no of Reserve tokens per user
    uint256[] public rewardOnProducts;// rewards on  each product 
    uint256 public durationOfCampaign;// duuration of campaign in seconds
    uint256 public campaignStartTime;// campaign start time when rules set
    uint256 public campaignEndTime;//campaign end time when rules set till duration added
    bool public campaignBySuperAdmin;// is campaign by super admin or not
    ReserveWallet wallet;// reserve wallet address to transfer reserve tokens to wallet
    enum campaignStatus {deployed, running, paused, ended }
    campaignStatus status;
    //campaign details in struct
    struct Campaign {
        uint256 id;
        address seller;
        uint256[] allocationOfTokens;
        uint256[] idOfProducts;
        uint256 zinTokens;
        uint256 reserveTokens;
    }
    //event fired when rules are set
    event rulesSet(uint256 gender, uint256 spendingCap, string _state, string _city,uint256 minAge,uint256 maxAge); 
    //event fired when offer availed
    event offerAvailed(address beneficiary, uint256 _ZinTokens,uint256 _reserveTokens);
    //event fired when Zin allocated to user
    event zinAvailed(address beneficiary,uint256 _tokens);
    //event fired when Reserve allocated to user
    event reserveAvailed(address beneficiary,uint256 _tokens);
    //event fired when Reserve tokens is Redeem 
    event reseveRedeem(address _beneficiary,uint256 _tokens);
    //event emited when campaign is stoped zin transfered to owner
    event campaignStoped(address _owner,uint256 zinTokens);
    //Eligiblity details
    struct Eligiblity 
    {
        uint256 gender; //0 for male,1 for female
        uint256 spendingCap;
        string state;
        string city;
        uint256 minAge;
        uint256 maxAge;
     
    }
    
    //eligiblityDetails of campaign using campaign id     
    mapping(uint256 => Eligiblity) public eligiblityDetails;
    //campaign details using campiagn ID
    mapping(uint256 => Campaign) public campaignDetails;
    //mapping product id in uint256 to bool,to check whether product is available in in campaign or not.
    mapping(uint256 => bool) public productExist;
    // mapping address to bool,to check if a particular user has already availed the offer of not.
    mapping(address => bool) public availedOffer;
    //product rewards against product ids
    mapping(uint256=>uint256) public productRewards;
    //reserve tokens availed by customer
    mapping(address => uint256) public reserveTokensCustomers;
    //zin tokens availed  by customers
    mapping(address => uint256) public zinCustomersTokens;

    uint256 public redeemedTokens;
    address public reserveWallet;
    uint256 public successTokens;
    address public superAdminAddress;

    constructor(address _owner, uint256[] _allocationOfTokens,uint256 zinTokens,uint256 reservedTokens, uint256[] productIds, uint256 _campaignId,uint256 duration,uint256[] _rewardOnProducts) Ownable(_owner) public
    {
     
     campaignDetails[_campaignId] = Campaign(
       {
            id: _campaignId,
            seller: _owner,
            allocationOfTokens : _allocationOfTokens,
            idOfProducts : productIds,
            zinTokens: zinTokens,
            reserveTokens: reservedTokens
       });
        campaignId = _campaignId;
        tokensLeft = zinTokens.add(reservedTokens);
        rewardOnProducts = _rewardOnProducts;
        durationOfCampaign=duration;
        status = campaignStatus.deployed;
    }
    //set rules for non super admin campaigns 
    //owner can set rules to avail offer 
    //gender => 0 for male,1 for female,2 for others
    //minimum speding cap to avail offers,state city will be use further,
    //minimum age to avail offer, maximum age to avail offer
    //reserveWallet address to manaage reserve wallet , minimu tokens to declare sucess of campaign
    function setRules( uint256 gender, uint256 spendingCap, string _state, string _city,uint256 minAge,uint256 maxAge,address _reserveWallet,uint256 _successTokens
    )  public onlyOwner returns(bool) {

        require(minAge < maxAge);
        eligiblityDetails[campaignId] = Eligiblity({
        
            gender:gender,
            spendingCap:spendingCap,
            state:_state,
            city:_city,
            minAge:minAge,
            maxAge:maxAge
        });
        campaignBySuperAdmin = false;// campaign is not by super admin
        reserveWallet = _reserveWallet;//initialised reserve wallet address
        wallet = ReserveWallet(reserveWallet);
        successTokens = _successTokens;//set success tokens
        campaignStartTime=now;//campaign start time
        campaignEndTime=now.add(durationOfCampaign);//campaign end time
        rewardAgainstProduct();//to set rewards against product
        status = campaignStatus.running;// update status of campaign
        emit rulesSet(gender,spendingCap,_state,_city,minAge,maxAge);
        return true;
    }

    //set rules for super admin campaigns 
    //owner can set rules to avail offer 
    //gender => 0 for male,1 for female,2 for others
    //minimum speding cap to avail offers,state city will be use further,
    //minimum age to avail offer, maximum age to avail offer
    //reserveWallet address to manaage reserve wallet , minimum tokens to declare sucess of campaign
    function setRulesBySuperAdmin( uint256 gender, uint256 spendingCap, string _state, string _city,uint256 minAge,uint256 maxAge,address _reserveWallet,uint256 _successTokens, address _superAdmin
    )  public onlyOwner returns(bool) {
        
        
                require(minAge < maxAge);
        eligiblityDetails[campaignId] = Eligiblity({
        
            gender:gender,
            spendingCap:spendingCap,
            state:_state,
            city:_city,
            minAge:minAge,
            maxAge:maxAge
        });
        campaignBySuperAdmin = true;//campaign is by super admin
        superAdminAddress = _superAdmin;
        reserveWallet = _reserveWallet;
        wallet = ReserveWallet(reserveWallet);
        successTokens = _successTokens;
        campaignStartTime=now;
        campaignEndTime=now.add(durationOfCampaign);
        rewardAgainstProduct();
        status = campaignStatus.running;
        emit rulesSet(gender,spendingCap,_state,_city,minAge,maxAge);
        return true;
    }

    // to avail offer, input exact age, gender, purchaseCapacity,address of beneficiary, product Id's  
    function availOffernow(uint256 _age, uint256 _gender,string _state,string _city, uint256 purchaseCapacity,address beneficiary ,uint256[] _productIds) isCampaignBySuperAdmin public returns (bool) 
    {
        //require(availedOffer[beneficiary ] == false);, Not in use now 
        require(isCampaignexists());//to check if campaign exits 
        require(status == campaignStatus.running); // check if not force fully stoped
        uint256  reserveTokens;
        uint256  zin;
        Campaign memory campaign = campaignDetails[campaignId];

        if(checkEligiblity(_age,_gender, purchaseCapacity,_state, _city))// to check eligibilty of user to avail offers
        {
            for(uint256 j=0; j < _productIds.length ;j++)
            {
                 uint256 currentId = _productIds[j];
                 
                if(productRewards[currentId] == 0)
                {
                    reserveTokens=reserveTokens.add(rewardOnProducts[j]);
                    availedOffer[beneficiary] = true;
                    wallet.allocateTokensToUser(beneficiary,rewardOnProducts[j]);// allocate reserve tokens to user in reserve wallet
                }
                else if(productRewards[currentId]==1) {
                    zin=zin.add(rewardOnProducts[j]);//no of zin tokens to send 
                    availedOffer[beneficiary] = true;
                }

            }

            tokensAvailed=tokensAvailed.add(reserveTokens).add(zin);//update tokens availed in this purchase
            require((zin <= campaign.zinTokens) && (reserveTokens <= campaign.reserveTokens));//check zin and resrve of campaign
            if(zin > 0){
                allocateZinToUsers(zin, beneficiary);//aloocate zin to user
            }
            if(reserveTokens > 0){
              allocateReserveToUsers(reserveTokens,beneficiary);//allocate resrve to campaign struct
            }

           
         
        }
        emit offerAvailed(beneficiary,zin,reserveTokens);//offer availed
      
    }

    // to check if offer canbe availed or not via product id
    // return 3 if both zin and reserve tokens are available,2 if only zin is available and 1 if reserve is available and 0 if none are aavailable
    function canAvailOffer(uint256[] _productIds) public view returns(uint256){
        uint256 reserveTokens;
        uint256 zin;
        for(uint256 j=0; j < _productIds.length ;j++)
            {
                 uint256 currentId = _productIds[j];
                 
                if(productRewards[currentId] == 0)
                {
                    reserveTokens=reserveTokens.add(rewardOnProducts[j]);
                    
                }
                else if(productRewards[currentId]==1) {
                    zin=zin.add(rewardOnProducts[j]);
                    
                }

        }
          Campaign memory campaign = campaignDetails[campaignId];
        if((campaign.zinTokens >= zin) && (campaign.reserveTokens >= reserveTokens)){
            return 3;
        } 
        else if((campaign.zinTokens >= zin) || (campaign.reserveTokens >= reserveTokens)) {
            if(campaign.zinTokens >= zin) {
            return 2;
            } else {
                return 1;
            }
        } else {
             return 0;
        }
    
    }

    //function to get reserve tokens left in campaign
    function reserveTokensLeft() public view returns(uint256) {
        Campaign memory campaign = campaignDetails[campaignId];
        return campaign.reserveTokens;
    }
    //function to get zin tokens left in campaign
    function zinTokensLeft() public view returns(uint256) {
        Campaign memory campaign = campaignDetails[campaignId];  
         return campaign.zinTokens;
    }
    
    // internal functions to allocate Reserve tokens in campaign
    function allocateReserveToUsers(uint256 reserve,address customer) internal {
        Campaign storage  campaign = campaignDetails[campaignId];
        uint256 existingReserve  = campaign.reserveTokens ;
        campaign.reserveTokens = existingReserve.sub(reserve);
        reserveTokensCustomers[customer] =  reserveTokensCustomers[customer].add(reserve);
        emit reserveAvailed(customer,reserve);
    }


    // internal functions to allocate Zin tokens 
    function allocateZinToUsers(uint256 zin,address customer) internal {
       Campaign storage campaign = campaignDetails[campaignId];
       campaign.zinTokens = campaign.zinTokens.sub(zin);
       uint256 zineum;
       zineum = zin*1 ether;
       zinCustomersTokens[customer] =  zinCustomersTokens[customer].add(zin);//zin tokens identified
       customer.transfer(zineum);//transfer zineum tokens to user
       emit zinAvailed(customer,zin);// emited zineum tokens is transfered to user
    }

    //  function to redeem Reserve Tokens of campaign 
    function redeemReserveTokens(address customer,uint256 tokens)  public {
        
        require(canRedeemTokens(customer,tokens));
        reserveTokensCustomers[customer] =  reserveTokensCustomers[customer].sub(tokens);
        redeemedTokens = redeemedTokens.add(tokens);
        emit reseveRedeem(customer,tokens);
    
    }
    // to check if campaign is Successful
    function iscampaignSuccessful() public view returns (bool) {
        
        if(successTokens < tokensAvailed) {
            return true; 
        }
        return false;
    }
    // to check Eligiblity of a customer to avail campaign
    function checkEligiblity(uint256 _age, uint256 _gender, uint256 purchaseCapacity,string _state, string _city) public view returns(bool) 
    {
        Eligiblity memory eligiblity  = eligiblityDetails[campaignId];
        
        if((_age > eligiblity.minAge) && (_age <= eligiblity.maxAge) && (eligiblity.gender == _gender) && (eligiblity.spendingCap <= purchaseCapacity) && (now >= campaignStartTime) && (now < campaignEndTime))
        {

            return true;// if all the details are true user is eligible

        }
        else {

        return false; // user not eligible

        }

    }
    
   // to set rewards against product id's    
   function rewardAgainstProduct() internal returns(bool) 
   {
        Campaign memory campaign = campaignDetails[campaignId];
        
        uint256[] memory _idOfProducts = campaign.idOfProducts;
        for(uint256 i=1; i<_idOfProducts.length; i=i+2)
            {
            if(_idOfProducts[i]==0) //Zero  for reserveTokens
                {
                    productRewards[_idOfProducts[i-1]] = 0;
                }
            else if(_idOfProducts[i]==1)
                {
                    productRewards[_idOfProducts[i-1]] = 1; //1 for Zin
                }
                
            }   
        return true;
   }
   
   //to check rewards against product id's
   function productHasRewards(uint256 _idOfProduct) public view returns(uint256) {
        return productRewards[_idOfProduct];
   }

    //to check rewards against product id's
    function rewardOnProduct(uint256 _idOfProduct)  public view returns(uint256) {
        return rewardOnProducts[_idOfProduct];
    }

    //to check if user can redeem particular token or not
    function canRedeemTokens(address user,uint256 tokens) public view returns(bool){
        
      
        if((reserveTokensCustomers[user] >= tokens)){
            return true;
        } else {
            return false;
        }
    }
    //to check if offer is availed by user not
    function isAvailed(address _beneficiary) public view returns(bool) {
        return availedOffer[_beneficiary];
    }
    //to check if offer is exits or not
    function isCampaignexists() public returns(bool)
    {
        if(now <= campaignEndTime)
        {
            return true;
        }
        else if (now>campaignEndTime)
        {
            status = campaignStatus.ended;
            return false;
        } 
    }
    // to check reserve tokens of a user    
    function userReserveTokens(address customer) public view returns(uint256) {
       return wallet.checkUserTokens(customer);
    }
    
    function () payable external {

    }
    //to check admin address if campaign By SuperAdmin
    function checkAdminAddress(address _superAdmin) public view returns(bool) 
    {
        require(superAdminAddress != 0x0,"superAdminAddress is null");
        return superAdmin(superAdminAddress).isAdmin(_superAdmin);
    }
    modifier isCampaignBySuperAdmin(){
      if(campaignBySuperAdmin == true)
      {
          
        require(checkAdminAddress(msg.sender));
        _;
      } else {
          _;
      }
  }
   
  // to stop Campaign, status will be pasued if forcefully stop   
  function StopCampaign() public onlyOwner{
      status = campaignStatus.paused;//update status
      uint256 ZintoTransfer =  zinTokensLeft() * 1 ether;
      msg.sender.transfer(ZintoTransfer);//transfer zin tokens to owner
      emit campaignStoped(msg.sender,ZintoTransfer);
  }
  // to get status ofa Campaign
  function getStatus() public view returns (string) {
    if (status == campaignStatus.deployed) return 'Campaign Deployed';
    else if (status == campaignStatus.paused) return 'Campaign Stoped';
    else if (status == campaignStatus.running) return 'Campaign Active';
    else if (status == campaignStatus.ended) return 'Campaign Ended';
    

  }

}


