pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;
pragma experimental "v0.5.0";
import "./SafeMath.sol";
import "./Ownable.sol";


contract ReserveWallet {
    function allocateTokensToUser(address user,uint256 tokens) public;
    function redeemUserTokens(address user,uint256 tokens) public;
    function checkUserTokens(address user) public view returns(uint256);
}

contract VendorCampaignforuser is Ownable 
{
    
    using SafeMath for uint256;
    uint256 public campaignId;
    uint256 public tokensLeft; 
    uint256 public minimumSuccessTokens;
    uint256 public tokensAvailed;
    uint256 public zinPerUser;
    uint256 public reserveTokensPerUser;
    uint256[] public rewardOnProducts;
    uint256 public durationOfCampaign;
    uint256 public campaignStartTime;
    uint256 public campaignEndTime;
    mapping (address => bool) public authorizedRetailer;
    ReserveWallet wallet;
    enum campaignStatus {deployed, running, paused, ended }
    campaignStatus status;
    
    struct Campaign {
        uint256 id;
        address seller;
        uint256[] allocationOfTokens;
        uint256[] idOfProducts;
        uint256 zinTokens;
        uint256 reserveTokens;  
    }

    event rulesSet(uint256 gender, uint256 spendingCap, string _state, string _city,uint256 minAge,uint256 maxAge); 
    event offerAvailed(address beneficiary, uint256 _ZinTokens,uint256 _reserveTokens);
    event zinAvailed(address beneficiary,uint256 _tokens);
    event reserveAvailed(address beneficiary,uint256 _tokens);
    event reseveRedeem(address _beneficiary,uint256 _tokens);
    event campaignStoped(address _owner,uint256 zinTokens);

    
    struct Eligiblity 
    {
       
        uint256 gender; //0 for male,1 for female
        uint256 spendingCap;
        string state;
        string city;
        uint256 minAge;
        uint256 maxAge;
     
    }
    

    mapping(uint256 => Eligiblity) public eligiblityDetails;
    mapping(uint256 => Campaign) public campaignDetails;
    //mapping product id in uint256 to bool,to check whether product is available in in campaign or not.
    mapping(uint256 => bool) public productExist;
    // mapping address to bool,to check if a particular user has already availed the offer of not.
    mapping(address => bool) public availedOffer;  
    mapping(uint256=>uint256) public productRewards;
    mapping(address => uint256) public reserveTokensCustomers;
    mapping(address => uint256) public zinCustomers;
    uint256 public redeemedTokens;

    address public reserveWallet;
    uint256 public successTokens;
    address public superAdminAddress;


    constructor(address _owner, uint256[] _allocationOfTokens,uint256 zinTokens,uint256 reservedTokens, uint256[] productIds, uint256 _campaignId,uint256 duration,uint256[] _rewardOnProducts) Ownable(_owner) public
    {
     
   
     campaignDetails[_campaignId] = Campaign(
       {
            id: _campaignId,
            seller: _owner,
            allocationOfTokens : _allocationOfTokens,
            idOfProducts : productIds,
            zinTokens: zinTokens,
            reserveTokens: reservedTokens
           
            
       });
        campaignId = _campaignId;
        tokensLeft = zinTokens.add(reservedTokens);
        rewardOnProducts = _rewardOnProducts;
        durationOfCampaign=duration;
        status = campaignStatus.deployed;
    }
    //authorise retailers to avail offer of users
    function authorizeRetailers(address[] _authorizedRetailer) public onlyOwner returns (bool success) {
        uint arrayLength = _authorizedRetailer.length;
        for (uint x = 0; x < arrayLength; x++) 
        {
            authorizedRetailer[_authorizedRetailer[x]] = true;
        }
        return true;
    }
    //to set rules to avail offer 
    //0 for male,1 for female,2 for others
    function setRules( uint256 gender, uint256 spendingCap, string _state, string _city,uint256 minAge,uint256 maxAge,address _reserveWallet,uint256 _successTokens
    )  public returns(bool) {
        eligiblityDetails[campaignId] = Eligiblity({
        
            gender:gender,
            spendingCap:spendingCap,
            state:_state,
            city:_city,
            minAge:minAge,
            maxAge:maxAge
        });
         
          reserveWallet = _reserveWallet;
          wallet = ReserveWallet(_reserveWallet);
          successTokens = _successTokens;
          campaignStartTime=now;
          campaignEndTime=now.add(durationOfCampaign);
          rewardAgainstProduct();
          status = campaignStatus.running;
          return true;
          emit rulesSet(gender,spendingCap,_state,_city,minAge,maxAge);
    }
    
   // new avail function
    function availOffernow(uint256 _age, uint256 _gender,string _state,string _city, uint256 purchaseCapacity,address beneficiary ,uint256[] _productIds) public returns (bool) 
    {
        //require(availedOffer[beneficiary ] == false);
        require(isCampaignexists());
        require(status == campaignStatus.running); 
        uint256  reserveTokens;
        uint256  zin;
    
        Campaign memory campaign = campaignDetails[campaignId];
        if(checkEligiblity(_age,_gender, purchaseCapacity,_state, _city))// add time parameters later on
        {
            for(uint256 j=0; j < _productIds.length ;j++)
            {
                 uint256 currentId = _productIds[j];
                 
                if(productRewards[currentId] == 0)
                {
                    reserveTokens=reserveTokens.add(rewardOnProducts[j]);
                    availedOffer[beneficiary] = true;
                    wallet.allocateTokensToUser(beneficiary,rewardOnProducts[j]);
                }
                else if(productRewards[currentId]==1) {
                    zin=zin.add(rewardOnProducts[j]);
                    availedOffer[beneficiary] = true;
                }

            }

            tokensAvailed=tokensAvailed.add(reserveTokens).add(zin);
            require((zin <= campaign.zinTokens) && (reserveTokens <= campaign.reserveTokens));
            if(zin > 0){
                allocateZinToUsers(zin, beneficiary);
            }
            if(reserveTokens > 0){
              allocateReserveToUsers(reserveTokens,beneficiary);
            }

           
         
        }  
       
        emit offerAvailed(beneficiary,zin,reserveTokens);
    }


    // return 3 if both zin and reserve tokens are available,2 if only zin is available and 1 if reserve is available and 0 if none are aavailable
    function canAvailOffer(uint256[] _productIds) public  view returns(uint256){
        uint256 reserveTokens;
        uint256 zin;
        for(uint256 j=0; j < _productIds.length ;j++)
            {
                 uint256 currentId = _productIds[j];
                 
                if(productRewards[currentId] == 0)
                {
                    reserveTokens=reserveTokens.add(rewardOnProducts[j]);
                    
                }
                else if(productRewards[currentId]==1) {
                    zin=zin.add(rewardOnProducts[j]);
                    
                }

        }
          Campaign memory campaign = campaignDetails[campaignId];
        if((campaign.zinTokens >= zin) && (campaign.reserveTokens >= reserveTokens)){
            return 3;
        } 
        else if((campaign.zinTokens >= zin) || (campaign.reserveTokens >= reserveTokens)) {
            if(campaign.zinTokens >= zin) {
            return 2;
            } else {
                return 1;
            }
        } else {
             return 0;
        }
    
    }

    //to check reserve token left in campaign
    function reserveTokensLeft() public view returns(uint256) {
        Campaign memory campaign = campaignDetails[campaignId];
        return campaign.reserveTokens;
    }
    //to check zin tokens left in campaign
    function zinTokensLeft() public view returns(uint256) {
        Campaign memory campaign = campaignDetails[campaignId];  
         return campaign.zinTokens;
    }
    //allocate reserve tokens to users    
    function allocateReserveToUsers(uint256 reserve,address customer) internal {
        Campaign storage  campaign = campaignDetails[campaignId];
        uint256 existingReserve  = campaign.reserveTokens ;
        campaign.reserveTokens = existingReserve.sub(reserve);
        reserveTokensCustomers[customer] =  reserveTokensCustomers[customer].add(reserve);
        emit reserveAvailed(customer,reserve);
    }
    //allocate Zin tokens to users
    function allocateZinToUsers(uint256 zin,address customer) internal {
       Campaign storage campaign = campaignDetails[campaignId];
       campaign.zinTokens = campaign.zinTokens.sub(zin);
       uint256 zineum;
       zineum = zin*1 ether;
       zinCustomers[customer] =  zinCustomers[customer].add(zin);
       customer.transfer(zineum);
       emit zinAvailed(customer,zin);
       
    }
    //redeem reserve tokens to users
    function redeemReserveTokens(address customer,uint256 tokens)  public {

        require(canRedeemTokens(customer,tokens));
        reserveTokensCustomers[customer] =  reserveTokensCustomers[customer].sub(tokens);
        redeemedTokens = redeemedTokens.add(tokens);
        emit reseveRedeem(customer,tokens);
    }
    //to check if campaign succesful or not    
    function iscampaignSuccessful() public view returns (bool) {
        
        if(successTokens < tokensAvailed) {
            return true; 
        }
        return false;
    }
    //to check if user is eligible or not
    //state , city not added yet to check elligibility 
    function checkEligiblity(uint256 _age, uint256 _gender, uint256 purchaseCapacity,string _state, string _city) public view returns(bool) 
    {
        Eligiblity memory eligiblity  = eligiblityDetails[campaignId];
        
        if((_age > eligiblity.minAge) && (_age <= eligiblity.maxAge) && (eligiblity.gender == _gender) && (eligiblity.spendingCap <= purchaseCapacity) && (now >= campaignStartTime) && (now < campaignEndTime)  &&  authorizedRetailer[msg.sender]==true)
        {
            return true;
        }
        return false;
    }
   //to set rewards against the product id's
   function rewardAgainstProduct() internal returns(bool) 
   {
        Campaign memory campaign = campaignDetails[campaignId];
        
        uint256[] memory _idOfProducts = campaign.idOfProducts;
        for(uint256 i=1; i<_idOfProducts.length; i=i+2)
            {
            if(_idOfProducts[i]==0) //Zero  for reserveTokens
                {
                    productRewards[_idOfProducts[i-1]] = 0;
                }
            else if(_idOfProducts[i]==1)
                {
                    productRewards[_idOfProducts[i-1]] = 1; //1 for Zin
                }
                
            }   
        return true;
   }
   // to check if ethereum address is a valid retailer or not
   function isRetailerValid(address _retailerAddress) public view returns(bool)
   {
     return authorizedRetailer[_retailerAddress];  
   }
   //to check rewards against product
   function productHasRewards(uint256 _idOfProduct) public view returns(uint256) {
        return productRewards[_idOfProduct];
   }
   //to check rewards against product   
    function rewardOnProduct(uint256 _idOfProduct)  public view returns(uint256) {
        return rewardOnProducts[_idOfProduct];
    }
    //to check if user can redeem tokens or not
    function canRedeemTokens(address user,uint256 tokens) public view returns(bool){
        
      
        if((reserveTokensCustomers[user] >= tokens)){
            return true;
        } else {
            return false;
        }
    }
    //to check if user availed offer not
    function isAvailed(address _beneficiary) public view returns(bool) {
        return availedOffer[_beneficiary];
    }
    //to check if campaign exits or not
    function isCampaignexists() public returns(bool)
    {
        if(now <= campaignEndTime)
        {
            return true;
        }
        else if (now>campaignEndTime)
        {
            status = campaignStatus.ended;
            return false;
        } 
    }
    // to check user reserve tokens in campaign    
    function userReserveTokens(address customer) public view returns(uint256) {
       return wallet.checkUserTokens(customer);
    }
   
    function () payable external {

    }
  //to stop campaign and transfer zin tokens to owner  
  function StopCampaign() public  onlyOwner{
      status = campaignStatus.paused;
      uint256 ZintoTransfer =  zinTokensLeft() * 1 ether;
      msg.sender.transfer(ZintoTransfer);
      emit campaignStoped(msg.sender,ZintoTransfer);
  }
  // to get the current status of campaign
  function getStatus() public view returns (string) {
    if (status == campaignStatus.deployed) return 'Campaign Deployed';
    else if (status == campaignStatus.paused) return 'Campaign Stoped';
    else if (status == campaignStatus.running) return 'Campaign Active';
    else if (status == campaignStatus.ended) return 'Campaign Ended';
    

  }


    
}


