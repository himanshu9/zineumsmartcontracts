pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

import "./SafeMath.sol";
import "./Ownable.sol";
import "./RetailerCampaignForUser.sol";

contract RetailerFactoryContract is Ownable 
{
    address public feeWallet;
    
    using SafeMath for uint256;
    //struct of campaign details
      struct Campaign {
        uint256 id;// campaignID
        address seller; //address of a seller 
        uint256 tokens;// campaign Zin tokens
        uint256 numOfProducts; // no of products in campaign
        uint256[] allocationOfTokens;// allocation of token on product zin or reserve in array
        uint256[] idOfProducts;//product id's in array
        uint[] rewardsOnProducts;//no of rewards on a single product in array
        uint256 zinTokens; // no of zin tokens of campaign
        uint256 reserveTokens;// no of reserve tokens in campaign
    }

    // mapping campaign address with campaign detail struct of campaign
    mapping(address => Campaign) public CampaignDetails;
    //address of campaign is valid or not
    mapping(address => bool) public campaigns;
    //licence mapping,returns true if licence for an address exists and false if licence is revoked or doesnot exist;
    mapping(address => bool) public licences;
    //maaping campaign id vs campaign address
    mapping(uint256 => address) public campaignIdVsAddress;
    // no of retailers have licence
    uint256 public numOfRetailers;
    //campaignID exits or not
    mapping(uint256 => bool) public campaignIdExits;
    
    // fee transfer event
    event feeTransfer(address _wallet, uint256 _amount);
    //zin transfer event 
    event zinTransfer(address _contract, uint256 _value);

    // modifier to check ethereum address have licence 
    modifier isLicenced(address ad) {
        require(licences[ad], "not licenced");
        _;
    }

    constructor() Ownable(msg.sender) public {}

    //to create retailer campaigns
    
    function createRetailerCampaign(
        uint256 campaignId, 
        uint256 _tokens,//  ryt now Token is no of Zin Tokens 
        uint256[] _allocationOfTokens,
        uint256[] productIds,
        uint256 duration,
        uint[] rewardsOnProducts
        
    ) payable isLicenced(msg.sender)  public returns(address)
    {
        require(feeWallet != address(0),"wallet address is null");//check fee wallet address is not null
        require(!campaignIdExits[campaignId]);//check campaign id doesnot exits
        uint256 reserveTokens = _allocationOfTokens[0];
        uint256 zinTokens =  _allocationOfTokens[1];
        //new reatile campaign is deployed
        address campaignAddress = new RetailerCampaignForUser(msg.sender,  _allocationOfTokens,zinTokens,reserveTokens,productIds,campaignId,duration,rewardsOnProducts); 
        campaigns[campaignAddress] = true;
        //campaign details mapped with struct
        CampaignDetails[campaignAddress] = Campaign({
            id: campaignId,
            seller: msg.sender,
            tokens: reserveTokens.add(zinTokens), 
            numOfProducts: productIds.length,
            allocationOfTokens : _allocationOfTokens,
            idOfProducts : productIds,
            rewardsOnProducts: rewardsOnProducts,
            zinTokens: zinTokens,
            reserveTokens:reserveTokens
        });
        //update campaign id valid
        campaignIdExits[campaignId] = true;
        //campaign id and address mapped
        campaignIdVsAddress[campaignId] = campaignAddress;
        //transfer zin as a fee to fee wallet 3 %
        feeWallet.transfer(msg.value.mul(3).div(100));
        //transfer zin to campaign 
        campaignAddress.transfer(msg.value.sub(msg.value.mul(3).div(100)));
        return campaignAddress;
        //fee transfer event emited
        emit feeTransfer(feeWallet,msg.value.mul(3).div(100));
        //Zin to campaign transfer event emited
        emit zinTransfer(campaignAddress,msg.value.sub(msg.value.mul(3).div(100)));
    }   
    //to set address to collect fees while creating campaign called by owner only
    function setFeeWallet(address _wallet) public onlyOwner returns(address)  {
        require(_wallet != 0x0,"wallet address is null");
        feeWallet = _wallet;
        return feeWallet;
    }
    
    //allocate licences to reatilers to create campaign
    function allocateLicence(address retailer) public onlyOwner{
      require(!licences[retailer]);
      licences[retailer] = true;
      numOfRetailers ++;
   }

    //revoke licences of reatilers 
   function revokeLicence(address retailer) public onlyOwner{
       require(licences[retailer]);
      licences[retailer] = false;
      numOfRetailers --;
   }

  //function will return address of a campaign by providing campaign id     
  function getAddressById(uint256 id) public view returns (address) {
      require(campaignIdExits[id]);
      return campaignIdVsAddress[id];
      
  }

   function () public{
       revert();
       
   }

  


}