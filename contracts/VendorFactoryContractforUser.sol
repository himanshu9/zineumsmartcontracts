pragma solidity 0.4.24;
pragma experimental ABIEncoderV2;

import "./SafeMath.sol";
import "./Ownable.sol";
import "./VendorCampaignforuser.sol";


 
contract VendorFactoryContractforUser is Ownable 
{
    address public feeWallet;
    
    
    
    using SafeMath for uint256;
      struct Campaign {
        uint256 id;
        address seller;
        uint256 tokens;
        uint256 numOfProducts;
        uint256[] allocationOfTokens;
        uint256[] idOfProducts;
        uint[] rewardsOnProducts;
        uint256 zinTokens;
        uint256 reserveTokens;
    }

    // mapping id of campaign to detail struct of campaign
    mapping(address => Campaign) public CampaignDetails;

    // campaign id to  bool;
    //address of campaign is the id of campaign;
    mapping(address => bool) public campaigns;
    event feeTransfer(address _wallet, uint256 _amount);
    event zinTransfer(address _contract, uint256 _value);
 
    //licence mapping,returns true if licence for an address exists and false if licence is revoked or doesnot exist;
    mapping(address => bool) public licences;

    mapping(uint256 => address) public campaignIdVsAddress;
    uint256 public numOfVendors;
 
    modifier isLicenced(address ad) {
        require(licences[ad], "not licenced");
        _;
    }

    constructor() Ownable(msg.sender) public {}
    //function to create campaign for users
    function createVendorCampaignforUser(
        uint256 campaignId, 
        uint256 _tokens,//  ryt now Token is no of Zin Tokens 
        uint256[] _allocationOfTokens,
        uint256[] productIds,
        uint256 duration,
        uint[] rewardsOnProducts
        
    ) payable isLicenced(msg.sender)  public  returns(address)
    {
        require(feeWallet != address(0),"wallet address is null");
        uint256 reserveTokens = _allocationOfTokens[0];
        uint256 zinTokens =  _allocationOfTokens[1];
        address campaignAddress = new VendorCampaignforuser(msg.sender,  _allocationOfTokens,zinTokens,reserveTokens,productIds,campaignId,duration,rewardsOnProducts); 
        campaigns[campaignAddress] = true;
        CampaignDetails[campaignAddress] = Campaign({
            id: campaignId,
            seller: msg.sender,
            tokens: reserveTokens.add(zinTokens), 
            numOfProducts: productIds.length,
            allocationOfTokens : _allocationOfTokens,
            idOfProducts : productIds,
            rewardsOnProducts: rewardsOnProducts,
            zinTokens: zinTokens,
            reserveTokens:reserveTokens
        });
        
        campaignIdVsAddress[campaignId] = campaignAddress;
        feeWallet.transfer(msg.value.mul(3).div(100));
        campaignAddress.transfer(msg.value.sub(msg.value.mul(3).div(100)));
        return campaignAddress;
        emit feeTransfer(feeWallet,msg.value.mul(3).div(100));
        emit zinTransfer(campaignAddress,msg.value.sub(msg.value.mul(3).div(100)));
    }   
    //to set fee wallet address to collect fees of creating campaign
    function setFeeWallet(address _wallet) public onlyOwner returns(address)  {
        require(_wallet != 0x0,"wallet address is null");
        feeWallet = _wallet;
        return feeWallet;
    }
    //function to allocate licences to vendors
    function allocateLicence(address vendor) public onlyOwner{
      require(!licences[vendor]);
      licences[vendor] = true;
      numOfVendors ++;
   }
    //function to revoke licences to vendors
   function revokeLicence(address vendor) public onlyOwner{
       require(licences[vendor]);
      licences[vendor] = false;
   }


    
   function (){
       revert();
       
   }

  


}