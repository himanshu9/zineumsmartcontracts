var RetailerFactoryContract  = artifacts.require("RetailerFactoryContract");
var RetailerCampaign = artifacts.require("RetailerCampaignForUser");
var superAdmin = artifacts.require("superAdmin");
var ReserveWallet = artifacts.require("ReserveWallet");

var Web3 = require("web3");
var web3 = new Web3("http://localhost:8545");
var factory;

contract("Retailer factory contract", function(accounts){


    it("should deploy factory .set wallet address in factory", async()=>{

        this.factory = await RetailerFactoryContract.new();
        await this.factory.setFeeWallet(accounts[1]);
        let wallet = await this.factory.feeWallet.call();
        assert.equal(accounts[1],wallet,"Wallet is not set");
    });

    it("should deploy Reserve wallet contract", async()=>{

        this.reserve = await ReserveWallet.new();
        //console.log(this.reserve.address,'reserve wallet');
    });

    it("should deploy Super Admin contract", async()=>{

        this.superAdmin = await superAdmin.new();
        //console.log(this.superAdmin.address,'Super admin');
    });

 
    it("should provide licence to a retailer", async()=>{

       await this.factory.allocateLicence(accounts[2]);
       let licenced =await this.factory.licences.call(accounts[2]);
       assert(licenced,true,"licence is not getting allocated");
    });


    it("should deploy a campaign", async()=>{
       let beforeWalletBalance =  await web3.eth.getBalance(accounts[1]);
       this.campaign =  await this.factory.createRetailerCampaign(1,11,[10,1],[1,0,2,1],100000,[1,1],{from:accounts[2],value:web3.utils.toWei("2","ether")});
       let walletBalance = await web3.eth.getBalance(accounts[1]);
       let gainedBalance = parseInt(walletBalance) - parseInt(beforeWalletBalance);
       assert.equal(gainedBalance,60000000000000000,"Fee transfer is wrong");
       var  showCampaignaddress = await this.factory.campaignIdVsAddress.call(1);
       this.campaignAddress = await this.factory.campaignIdVsAddress.call(1);
       let campaignBalance = await web3.eth.getBalance(this.campaignAddress);
       assert.equal(campaignBalance,1940000000000000000,"campaign contract balance is wrong");
    });


    it("should be able to set eligiblity of campaign", async()=>{
        
        var  showCampaignaddress = await this.factory.campaignIdVsAddress.call(1);
        this.campaignAddress = await this.factory.campaignIdVsAddress.call(1);
        this.retailerCampaignContract = RetailerCampaign.at(this.campaignAddress);
        this.retailerCampaignContract.setRules(0,100,"delhi","delhi",21,24,this.reserve.address,100,{from:accounts[2]})
        let details = await this.retailerCampaignContract.eligiblityDetails.call(1);
        var zinTokensLeft = await this.retailerCampaignContract.zinTokensLeft();
        //console.log(zinTokensLeft.toNumber());
    });

    
    it("should be able to check zin or reserve tokens on a specific product ", async()=>{
       
        let reward = await this.retailerCampaignContract.productHasRewards.call(2);
        assert.equal(reward.toNumber(),1,"product reward is wrong");
    });

   it("should be able to check eligibility to participate in campaign ", async()=>{
         
       
        let _value = await this.retailerCampaignContract.checkEligiblity(23,0,100,"delhi","delhi",{from:accounts[2]});
        assert.equal(_value,true,"not eligible");   
    });

    it("should be able to avail offer in campaign ", async()=>{
        let campaignBySuperAdmin = await this.retailerCampaignContract.campaignBySuperAdmin.call();
        //console.log(campaignBySuperAdmin.toString(),'campaign by super admin'); 
        let beforeCustomerBalance =  await web3.eth.getBalance(accounts[5]);
        let beforeContractBalance =  await web3.eth.getBalance(this.retailerCampaignContract.address);
        //console.log(beforeCustomerBalance,'Balance of a customer before');
        //console.log(beforeContractBalance,'Contract balance before availing offer');
        let _value = await this.retailerCampaignContract.availOffernow(23,0,"delhi","delhi",100,accounts[5],[1,2],{from:accounts[2]});
        let afterCustomerBalance =  await web3.eth.getBalance(accounts[5]);
        let beforeContractBalance1 =  await web3.eth.getBalance(this.retailerCampaignContract.address);
        //console.log(afterCustomerBalance,'after availing offer customer balance ');
        //console.log(beforeContractBalance1,'after availing offer contract balance');
        let isAvailed = await this.retailerCampaignContract.isAvailed.call(accounts[5]);
        assert.equal(isAvailed,true,"offer is availed");
        let ZinCustomer =  await this.retailerCampaignContract.zinCustomers.call(accounts[5]);
        //console.log(ZinCustomer.toNumber(),'Zin customer Balance');
        let userReserve = await this.retailerCampaignContract.userReserveTokens(accounts[5]);
        let tokensLeft =  await this.retailerCampaignContract.reserveTokensLeft.call();
        assert.equal(tokensLeft.toNumber(),9,"tokens left is wrong");
        assert.equal(userReserve.toNumber(),1,"user reserve is wrong");

  });

    it("should redeem Reserved tokens from user", async()=>{
        await this.retailerCampaignContract.redeemReserveTokens(accounts[5],1,{from:accounts[5]});
        let userReserve = await this.retailerCampaignContract.userReserveTokens(accounts[5]);
        let redeemedTokens = await this.retailerCampaignContract.redeemedTokens.call();
        assert.equal(userReserve.toNumber(),1,"user reserve is wrong");
        assert.equal(redeemedTokens.toNumber(),1,"redeemed tokens are wrong");

     });  
});

